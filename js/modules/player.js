var player = {
	details: function(name,userName,email){
		this.name = name || {} ;
		this.userName = userName || {};
		this.email = email || {};
		console.log("Name: " + this.name + "\nUsername: " + this.userName + "\nEmail: " + this.email);
		return this;
	},
	score: function(score){
		this.score = score || 0;
		console.log(this.userName + ' has a score of: ' + this.score);
		return this;
	},
	rank: function(heroType){
		this.heroType = heroType;
		var rank;

		if(this.heroType === 'palidon'){
			if((this.score >= 1000 && this.score < 2000) && (this.heroType === "palidon")) {
				rank = 'scribe';
				console.log(this.userName + " is currently ranked, " + rank + " and plays as, " + this.heroType);
			} else if((this.score >= 2000 && this.score < 3000) && (this.heroType === "palidon")) {
				rank = 'aprentace';
				console.log(this.userName + " is currently ranked, " + rank + " and plays as, " + this.heroType);
			} else if((this.score >= 3000 && this.score < 4000) && (this.heroType === "palidon")) {
				rank = 'soldier';
				console.log(this.userName + " is currently ranked, " + rank + " and plays as, " + this.heroType);
			} else if((this.score >= 4000 && this.score < 5000) && (this.heroType === "palidon")) {
				rank = 'champion';
				console.log(this.userName + " is currently ranked, " + rank + " and plays as, " + this.heroType);
			} else if(this.score > 5000 && this.heroType === "palidon") {
				rank = 'legion';
				console.log(this.userName + " is currently ranked, " + rank + " and plays as, " + this.heroType);
			} else {
				rank = 'noob - under 1k';
				console.log(this.userName + " is currently ranked, " + rank + " and plays as, " + this.heroType);
			}
		} else {
			if((this.score >= 1000 && this.score < 2000) && (this.heroType === "spellcaster")) {
				rank = 'wizard';
				console.log(this.userName + " is currently ranked, " + rank + " and plays as, " + this.heroType);
			} else if((this.score >= 2000 && this.score < 3000) && (this.heroType === "spellcaster")) {
				rank = 'cleric';
				console.log(this.userName + " is currently ranked, " + rank + " and plays as, " + this.heroType);
			} else if((this.score >= 3000 && this.score < 4000) && (this.heroType === "spellcaster")) {
				rank = 'droid';
				console.log(this.userName + " is currently ranked, " + rank + " and plays as, " + this.heroType);
			} else if((this.score >= 4000 && this.score < 5000) && (this.heroType === "spellcaster")) {
				rank = 'sorcerer';
				console.log(this.userName + " is currently ranked, " + rank + " and plays as, " + this.heroType);
			} else if(this.score >= 5000 && this.heroType === "spellcaster") {
				rank = 'overlord';
				console.log(this.userName + " is currently ranked, " + rank + " and plays as, " + this.heroType);
			} else {
				rank = 'noob - under 1k';
				console.log(this.userName + " is currently ranked, " + rank + " and plays as, " + this.heroType);
			}
		}
	}
}

