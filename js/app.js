//player objects
console.log('\n');
var Torean = Object.create(player);
Torean.details('Torean','The.RaVeN','name@domain.com').score(2500).rank("palidon");

console.log('\n');
var Mesmer = Object.create(player);
Mesmer.details('Lian','Mesmer','name@domain.com').score(8000).rank("palidon");

console.log('\n');
var Stevie = Object.create(player);
Stevie.details('Stevie','The.Qrow','name@domain.com').score(4500).rank("spellcaster");

console.log('\n');
var Jurie = Object.create(player);
Jurie.details('Jurie','Murky.twitch.tv','name@domain.com').score(1200).rank("spellcaster");