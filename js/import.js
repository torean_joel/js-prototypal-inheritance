//prototypal
var module = {
	require: function(url){
		this.dom = document.createElement('script');
		this.dom.src = url;
		this.head = document.getElementsByTagName('head')[0];

		var head = this.head
		document.head.appendChild(this.dom);
		return this;
	}
}
//module scripts that hold the main methods of new called objects
module.require('js/modules/player.js').require('js/modules/script3.js').require('js/modules/script2.js').require('js/modules/script4.js').require('js/modules/script5.js');
module.require('js/modules/script6.js').require('js/modules/script7.js').require('js/modules/script8.js').require('js/modules/script9.js').require('js/modules/script10.js');

//the module where all the code get initiated - here i run the code from the modules
module.require('js/app.js');